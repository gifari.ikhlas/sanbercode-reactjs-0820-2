import React, { Component } from "react"
/* import "./Lists.css" */

class Form extends Component {

    constructor(props) {
        super(props)
        this.state1 = {

            dataNama: ["semangka", "anggur", "strawberry", "jeruk", "mangga"],
            inputNamaBuah: ""


        }
        this.state2 = {
            dataHarga: [10000, 40000, 30000, 30000, 30000],
            inputHarga: ""

        }
        this.state3 = {
            dataBerat: [1000, 500, 400, 1000, 500],
            inputBerat: ""
        }

        this.handleChange1 = this.handleChange1.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.handleChange3 = this.handleChange3.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange1(event) {
        this.setState({ inputNamaBuah: event.target.value });


    }
    handleChange2(event) {
        this.setState({ inputHarga: event.target.value });
    }
    handleChange3(event) {
        this.setState({ inputBerat: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault()
        console.log(this.state.inputNamaBuah)
        console.log(this.state.inputHarga)
        console.log(this.state.inputBerat)
        this.setState({
            pesertaLomba: [...this.state1.dataNama, this.state.inputNamaBuah],
            inputNamaBuah: ""


        })
        this.setState({
            pesertaHarga: [...this.state2.dataHarga, this.state.inputHarga],
            inputName: "",

        })
        this.setState({
            pesertaLomba: [...this.state3.dataBerat, this.state.inputBerat],
            inputBerat: ""
        })
    }

    render() {
        return (
            <>
                <h1>Daftar harga buah</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            {
                                this.state1.dataNama.map((val) => {
                                    return (

                                        <td>{val}</td>

                                    )
                                })

                            }
                            {
                                this.state2.dataHarga.map((harga) => {
                                    return (

                                        <td>{harga}</td>

                                    )
                                })
                            }
                            {
                                this.state3.dataBerat.map((berat) => {
                                    return (
                                        <td>{berat}</td>
                                    )
                                })
                            }
                        </tr>

                    </tbody>
                </table>
                {/* Form */}
                <h1>Form Peserta</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Masukkan nama buah:
          </label>
                    <input type="text" value={this.state1.inputNamaBuah} onChange={this.handleChange1} />
                    <label>
                        Masukkan Harga Buah:
          </label>
                    <input type="text" value={this.state2.inputHarga} onChange={this.handleChange2} />
                    <label>
                        Masukkan Berat Buah:
          </label>
                    <input type="text" value={this.state3.inputBerat} onChange={this.handleChange3} />
                    <button>submit</button>
                </form>
            </>
        )
    }
}

export default Form