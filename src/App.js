import React from 'react';
import './App.css';
import Tugas9 from './Tugas-9/tugas9.js';
import Tugas10 from './Tugas-10/tugas10.js';

import Form from './Tugas-12/tugas12.js';

function App() {
  return (
    <div>
        <Tugas9 />
        <Tugas10/>
        <Form/>
    </div>
  );
}

export default App;
